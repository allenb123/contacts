# Contacts
A text-based file format for storing contacts.

[Format](FORMAT.md)

## Usage
See
```
contacts -h
```

## Building
```bash
go build -o contacts *.go
```