# `contacts.txt` format
```
Full Name: [Me]
 [Key: ] Value
 +
```

Where `Value` is one of:

 * `+[0123456789-]*` (Phone)
 * `*@*.*` (Email)
 * `Jan 01, 2018` (`Birthday`)
 * `*`

## Official Keys
`Phone` - default key for phone number  
`Email` - default key for email  
`Address` - address  
`Birthday` - key for birthday

## Example
```
Elmo:
 +1-844-872-4681
 elmo@example.org
 Hotline: +1-800-273-8255
 Work: elmo_work@example.com
 Address: 1 Sesame Street, ....
 Notes: Very immature
```