package contacts

import (
	"io"
	"strings"
	"bufio"
	"errors"
	"strconv"
	"time"
)

type Contact struct {
	Name string
	properties map[string]interface{}
}


type ContactList map[string][]*Contact

func (c *Contact) String() string {
	return c.Name
}

func NewContact() *Contact {
	out := new(Contact)
	out.properties = make(map[string]interface{})
	return out
}

func Load(in io.Reader) (ContactList, error) {
	scanner := bufio.NewScanner(in)

	out := make(ContactList)

	var current *Contact = nil
	var currentCircle = ""
	for scanner.Scan() {
		text := scanner.Text()
		if len(strings.Trim(text, " \t")) == 0 {
			continue
		}

		if text[0] == '[' && text[len(text) - 1] == ']' {
			// New circle
			if current != nil {
				out[currentCircle] = append(out[currentCircle], current)
			}
			current = nil

			currentCircle = text[1:len(text) - 1]
		} else if text[0] != ' ' {
			// New person

			// Store previous person
			if current != nil {
				out[currentCircle] = append(out[currentCircle], current)
			}

			current = NewContact()
			del := strings.Index(text, ":")
			if del != -1 {
				current.Name = text[0:del]
			} else {
				current.Name = text
			}
			continue
		} else {
			text = strings.Trim(text, " \t")
			if current == nil {
				return out, errors.New("No person")
			}

			del := strings.Index(text, ":")
			var key, val string
			if del == -1 {
				val = text
			} else {
				key = strings.Trim(text[0:del], " \t")
				val = strings.Trim(text[del + 1:], " \t")
			}

			// Parse value
			if val[0] == '+' {
				// Phone #
				if len(key) == 0 {
					key = "Phone"
				}
				num, err := strconv.ParseUint(strings.NewReplacer("+", "", "-", "", " ", "", "\t", "").Replace(val), 10, 64)
				if err != nil {
					return out, err
				}
				current.properties[key] = uint64(num)
			} else {
				if key == "" && strings.Contains(val, "@") {
					key = "Email"
				}
				// Anything else
				current.properties[key] = val
			}
		}
	}

	return out, nil
}

func (c* Contact) Birthday() (time.Time, error) {
	str, ok := c.properties["Birthday"].(string)
	if !ok {
		return time.Now(), errors.New("No birthday specified")
	}
	day, err := time.Parse("Jan 02, 2006", str)
	return day, err
}

func (c* Contact) Get(prop string) interface{} {
	return c.properties[prop]
}

func (c* Contact) Phones() map[string]uint64 {
	out := make(map[string]uint64)
	for key, val := range c.properties {
		switch val.(type) {
			case uint64:
				out[key] = val.(uint64)
		}
	}
	return out
}

func (c* Contact) Address() string {
	val, ok := c.properties["Address"].(string)
	if !ok {
		return ""
	} else {
		return val
	}
}

func (c* Contact) Emails() map[string]string {
	out := make(map[string]string)
	for key, val := range c.properties {
		switch val.(type) {
			case string:
				if strings.Contains(val.(string), "@") {
					out[key] = val.(string)
				}
		}
	}
	return out
}

func (c* Contact) Notes() string {
	val, ok := c.properties["Notes"].(string)
	if !ok {
		return ""
	} else {
		return val
	}
}