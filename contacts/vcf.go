package contacts

import (
	"strings"
	"fmt"
)

func VCF(item *Contact) string {
	var out = ""
	out += "BEGIN:VCARD\n"
	out += "VERSION:4.0\n"
	out += "FN:" + item.Name + "\n"

	for key, phone := range item.Phones() {
		out += "TEL;VALUE=uri"
		if key == "Mobile" {
			key = "Cell"
		}
		if key != "Phone" {
			out += ";TYPE=" + strings.ToLower(key)
		}
		out += ":+" + fmt.Sprint(phone) + "\n"
	}

	if len(item.Address()) != 0 {
		out += "ADR;LABEL=\"" + item.Address() + "\":;;;;;\n"
	}

	for key, email := range item.Emails() {
		out += "EMAIL"
		if key != "Email" {
			out += ";TYPE=" + strings.ToLower(key)
		}
		out += ":" + email + "\n"
	}

	if item.Get("Gender") != nil {
		letter, ok := map[string]string {
			"Male":"M",
			"Female":"F",
			"Other":"O",
		}[fmt.Sprint(item.Get("Gender"))]
		if !ok {
			letter = "U"
		}
		out += "GENDER:" +  letter + ";" + fmt.Sprint(item.Get("Gender")) + "\n"
	}

	if item.Get("Notes") != nil {
		out += "NOTE:" + fmt.Sprint(item.Get("Notes")) + "\n"
	}

	if bday, err := item.Birthday(); err == nil {
		out += "BDAY:" + bday.Format("20060102") + "\n"
	}
	
	out += "END:VCARD" + "\n"

	return out
}