package main

import (
	"fmt"
	"./contacts"
	"os"
	"flag"
)

var format string
var mode string

func init() {
	flag.StringVar(&format, "fmt", "terminal", "Type of output [terminal|vcf]")
	flag.StringVar(&mode, "m", "short", "Mode [list|short]")
}

func printTerminal(items contacts.ContactList) {
	for circle, list := range items {
		if len(circle) == 0 {
			circle = "\033[32mOther\033[39m"
		}
		fmt.Println("\033[1m" + circle + "\033[22m")
		for _, contact := range list {					
			if mode == "short" {
				fmt.Print(" ", contact.Name)

				/* Get phone */
				phone, ok := contact.Phones()["Phone"]
				if !ok {
					phone = 0
					for _, phone = range contact.Phones() { break }
				}

				if phone != 0 {
					fmt.Println(": \033[33m+" + fmt.Sprint(phone) + "\033[39m")
					continue				
				}
				
				/* Get email if no phone */
				email := ""
				for _, email = range contact.Emails() { break }
				if len(email) != 0 {
					fmt.Println(": \033[31m" + email + "\033[39m")
					continue
				}

				fmt.Println()
			} else { // mode == "list"
				fmt.Println(contact.Name)
			}
		}
	}
}

func printVcf(items contacts.ContactList) {
	for _, list := range items {
		for _, contact := range list {
			fmt.Println(contacts.VCF(contact))
		}
	}
}

func main() {
	flag.Parse()

	filename := os.Getenv("HOME") + "/contacts.txt"
	if len(flag.Arg(0)) != 0 {
		filename = flag.Arg(0)
	}
	data, err := os.Open(filename)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
		return
	}
	items, err := contacts.Load(data)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
		return
	}
	switch format {
	case "terminal":	
		printTerminal(items)
		break
	case "vcf":
		printVcf(items)
	default:
		fmt.Fprintln(os.Stderr, "Unknown output type")
		os.Exit(1)
	}
}
